{ |==========================================================|
|
|                  G V S O F T                                           |
|                  Projet : GVLogo                                       |
|                  Description : Listes de propri�t�s                    |
|                  Unit� : GVPropLists.pas                               |
|                  Ecrit par  : VASSEUR Gilles                           |
|                  e-mail : g.vasseur58@laposte.net                      |
|                  Copyright : � G. VASSEUR                              |
|                  Date:    08-08-2014 21:39:46                          |
|                  Version : 1.0.0                                       |
|                                                                        |
  |============================================================| }

// GVPropLists - part of GVLOGO
// Copyright (C) 2014 Gilles VASSEUR
//
// This program is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program.
//  If not, see <http://www.gnu.org/licenses/>.

// Unit� pour le traitement des listes de propri�t�s
//
// ##############################################################
//
// Une liste de propri�t�s associe des valeurs � des caract�ristiques
// choisies pour un objet.
// Ainsi, un chien pourra �tre d�fini par les propri�t�s : race, �ge, sexe.
// Chacune de ces propri�t�s aura une valeur particuli�re.
//
// Les listes de propri�t�s sont elles-m�mes � la base du noyau de
// l'interpr�teur GVLOGO.
//