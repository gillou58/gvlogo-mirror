{ |===========================================================
  |                  G V S O F T
  |                  Projet : GVLogo micro-logiciel EASYTURTLE
  |                  Description : fiche principale
  |                  Unit� : Main.pas
  |                  Ecrit par  : VASSEUR Gilles
  |                  e-mail : g.vasseur58@laposte.net
  |                  Copyright : � G. VASSEUR
  |                  Date:    08-08-2014 12:29:48
  |                  Version : 1.0.0
  |}

// logiciel de dessin avec la tortue GVLOGO, destin� � un jeune public.
// le dossier "samples" contient des exemples de dessins.
//
// EasyTurtle - part of GVLOGO
// Copyright (C) 2014 Gilles VASSEUR
//
// This program is free software: you can redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program.
// If not, see <http://www.gnu.org/licenses/>.