﻿GVLOGO
======

French LOGO language with LAZARUS (Free Pascal)

Implémentation de LOGO avec LAZARUS (Free Pascal)


************************************************

Logiciel GVLOGO - V1.0.0 - Gilles VASSEUR 2014

Sommaire :

1. Objectifs du travail présenté
2. LICENCE
3. Contenu des répertoires
4. Contact

************************************************

I. Objectifs du travail présenté

GVLOGO est un logiciel qui implémente pour Windows (32 et 64 bits) et Linux une version du langage LOGO. Il est par ailleurs fourni avec tous les programmes sources et les utilitaires nécessaires à la compréhension de son fonctionnement.

Le langage utilisé pour son développement est le PASCAL. La réalisation utilise le logiciel libre LAZARUS qui repose lui-même sur FREEPASCAL. Ce sont deux produits suffisamment stables pour intéreser tout programmeur. Qui plus est, ils sont entièrement gratuits.

Les objectifs sont donc :
* offrir une version gratuite et opérationnelle d'un dialecte de LOGO.
* permettre à un programmeur inexpérimenté de prendre conscience des difficultés rencontrées lors de la mise en oeuvre d'un tel projet.


II. LICENCE

Les documents et logiciels du dossier GVLOGO sont protégés selon les termes de la licence GNU GPL qui doit être présente dans le dossier "0- Licence GNU GPL".

a. Version anglaise valable juridiquement :

GVLOGO - logiciel  de programmation
Copyright (C) 2014 Gilles VASSEUR — Tous droits réservés.

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

b. Version française pour information :

GVLOGO - logiciel  de programmation
Copyright (C) 2014 Gilles VASSEUR — Tous droits réservés.
  
  Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la “GNU General Public License” telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
  
  Ce programme est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.
  
  Vous devriez avoir reçu une copie de la Licence Générale Publique GNU avec ce programme ; si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.

III. Contenu des répertoires

Répertoire GNU GPL : contient la licence GNU GPL sous plusieurs formats, en anglais et en français.

Répertoire medias : contient les images nécessaires pour les programmes, mais aussi pour la documentation.

Répertoire docs : contient la documentation de GVLOGO (programmation et mode d'emploi.

Répertoire tests : contient tous les logiciels de tests des unités de GVLOGO.
   => testgvwords : test des mots
   
   => testgvlists : test des listes
   
   => testgvproplists : test des listes de propriétés
   
   => testgvturtles : test de la tortue graphique (version avec outils de base fournis)
   
   => testgvturtles2 : test de la tortue graphique (version avec la bibliothèque BGRABITMAP)
   
   => testeasyturtle : programme de dessin pour enfants (test approfondi de la tortue graphique en version simple)
   
   => testeasyturtle2 : programme easyturtle amélioré (avec gvturtles2).
   

Répertoire units : contient toutes les unités nécessaires à GVLOGO.


IV. Contact

Vous pouvez me contacter par mail : gillesvasseur58@gmail.com

Vous pouvez aussi vous rendre sur mon site pour les éventuelles mises à jour : www.lettresenstock.org

Les suggestions, modifications, propositions et questions sont les bienvenues.

Gilles VASSEUR, le 4 septembre 2014

 
